# Vagrant exercise #2

## Objective
Copy vagrantfiles from previous exercise and:
- assign them private IPs:
	- for ubuntu IP Range is 10.19.20.1-10.19.20.254
	- for centos IP Range is 192.168.19.1-192.168.19.254
- add them shared directory: C:\dev\
- add inline shell provisioning and install nginx and htop packages