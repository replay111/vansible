# Vagrant exercise #3

## Objective
Copy vagrantfiles from previous exercise and:
- Generate private and public on your windows machine
- Insert your public key to these vms by using vagrantfile
- Test whether you can connect to these vms directly by using their ip and your public key