# Ansible exercise #4
Jinja2 Templates

## Objective
- Deploy attached html files to nginx web dir on node 2 & node 3
- Create template file based on attached nginx configuration 
- Define variables for nginx loadbalancer
- Update node 1 nginx configuration with the output produced by the template and variables
- Define variables needed for deploying attached ssh banner file
- Create template based on the banner file
- Deploy the banner file to node 1 and 2

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2