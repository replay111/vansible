# Ansible exercise #10
Molecule

## Objective
- Install python-pip
- Install virtualenv python pkg
- Create virtual environment & activate it
- Install molecule and docker inside it
- Initialize a role with docker driver
- Get into it and run tests

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2