# Ansible exercise #8
Role 1 - Galaxy

## Objective
- Create Ansible role directory tree
- Try to write a role with **at least**:
    - two handlers,
    - two variable files (defaults & based on distribution),
    - one template,
    - one file,
    - three conditionals,
    - simple meta/main.yml file.

Use (centos & ubuntu) vagrantfiles from Ansible exercise #2