# Ansible exercise #1
Installation and initial configuration 

## Objective
- Install and configure ansible and ansible mitogen (choose your preffered os)
- Add the other hosts to ansible hosts
- Exchange SSH keys
- Execute ansible ping

Use (centos or ubuntu) vagrantfiles from Vagrant exercise #4