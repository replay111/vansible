# Ansible exercise #3
Ansible CLI

## Objective
- CLI:
	- download file http://www.ovh.net/files/10Mb.dat to master host
	- send this file to all nodes in your cluster
	- add user ansible with your public key on all nodes in your cluster
	- add user test on all nodes in your cluster
	- as user ansible download the same file and place it in ansible home directory on each node 
	- remove installed htop package from all nodes in your cluster
	- remove test user
	- stop nginx service and use curl and services module to verify that it has been stopped
	- finally, reboot all of the machines

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2