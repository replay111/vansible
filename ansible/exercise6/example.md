# Exercise 6

## Both (Ubuntu & CentOS):

### Playbook

```yaml
---
- name: Example playbook
  hosts: all
  become: no

  tasks:
    - name: Print ansible_facts
      debug:
        msg: "{{ hostvars }}"

    - name: Get nginx version
      shell: "nginx -v 2>&1 | awk -F/ '{print $2}'"
      register: version

    - name: Create CSV report
      shell: >
        "echo HOSTNAME: {{ hostvars[inventory_hostname]['ansible_nodename'] }},DISTRO: {{ hostvars[inventory_hostname]['ansible_distribution'] }},IP: {{ hostvars[inventory_hostname]['ansible_default_ipv4']['address'] }}, TIME: {{ hostvars[inventory_hostname]['ansible_date_time']['time'] }}, VERSION: {{ version.stdout }} >> report.csv"
      delegate_to: localhost

```