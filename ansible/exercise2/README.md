# Ansible exercise #2
Variables and parameters, inventory parameters

## Objective
- Split your cluster into 2 groups and a master host in ansible hosts
- Define for each group default download directory
- Write playbook that will download file from url given in cli parameter - the link is [http://www.ovh.net/files/10Mb.dat](http://www.ovh.net/files/10Mb.dat)
- Store that file into download directory defined in ansible hosts


Use (centos or ubuntu) vagrantfiles from Ansible exercise #1