# Exercise 9

## Both (Ubuntu & CentOS):

### CLI

```sh
# Search for apache2 (httpd) role
ansible-galaxy search apache2
# or
ansible-galaxy search httpd
# or filter by tags
ansible-galaxy search --galaxy-tags=apache

# Install role
ansible-galaxy install adfinis-sygroup.apache

# By default, roles are being stored in $HOME/.ansible/roles/

# Copy role into your workspace
cp -r $HOME/.ansible/roles/adfinis-sygroup.apache my_workspace/roles/

# Customize it
nano my_workspace/roles/adfinis-sygroup.apache/tasks/main.yml

```

### Try to run it

```yaml
---
- name: Example playbook
  hosts:
  roles:
    - adfinis-sygroup.apache
    - foo # You can run multiple roles at once (one after another)
    - bar

```

#### Help

- You can help yourself by seeing the [docs](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html).

- Feel free to ask any questions. :grey_question: :wink: