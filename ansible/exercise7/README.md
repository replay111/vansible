# Ansible exercise #7
File editing

## Objective
- Add 127.0.1.1 address to hosts file
```
127.0.1.1 _hostname_
```
- Insert a comment right before _user_ statement in nginx config file if absent
- Create a variable in /etc/environment file
- Create an alias called _myinfo_ in $HOME/.bashrc file which returns _user, shell_

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2